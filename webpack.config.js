const path = require('path');

module.exports = {
  mode: "development", // "production" | "development" | "none"
  
  entry: "./assets/js/main.js", 
 
  output: {
  
    path: path.resolve(__dirname, "dist"), 
    filename: "bundle.js",

  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  }
}